package monpackage;




import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Anne
 */
public class GetWayCategorie {
  private   Connection conn;
    private PreparedStatement allCategorieProduitInfo, allProduitInfo;
    private ResultSet result;
    private String url;
     private String user;
    private  String pw;
    
    
    
    public GetWayCategorie(String url,String user,String pw) 
    {
        try
        {
            //com.mysql.cj.jdbc.Driver
           // Class.forName("com.mysql.jdbc.Driver");
             Class.forName("com.mysql.jdbc.Driver");
            this.url = url;
            this.user = user;
            this.pw = pw;
            try
            {
                
                this.conn = DriverManager.getConnection(this.url, this.user, this.pw);
                this.allCategorieProduitInfo = conn.prepareStatement("select * from produit where id_categorie=?;");
                allProduitInfo = conn.prepareStatement("select * from produit where nom=?");
                
            }
            catch (SQLException ex) 
            {
                Logger.getLogger(GetWayCategorie.class.getName()).log(Level.SEVERE, null, ex);
            }
           
        } 
        catch (ClassNotFoundException ex)
        {
            Logger.getLogger(GetWayCategorie.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public ArrayList<Produit> getListCatFromProduit(String cat) {
      //  int taille=3;
           ArrayList<Produit> listOfProduitFromCat;
      listOfProduitFromCat = new ArrayList<Produit>();
             try{
                 this.allCategorieProduitInfo.setString(1,cat);
                 result=this.allCategorieProduitInfo.executeQuery();
                 while(result.next()){
                   
listOfProduitFromCat.add(new Produit(result.getInt("id"),result.getString("nom"),result.getString("description"),
                                  result.getString("image"),result.getInt("prix")));
                 }
             }
             
       catch (SQLException ex)
        {
            System.out.println(ex.getMessage());
        }
               
    return listOfProduitFromCat ;
}
    
    public ArrayList <Produit> getListProduit(String nomprod) {
             ArrayList<Produit> listOfProduit =new ArrayList<Produit>();
             try{
                 this.allProduitInfo.setString(1,nomprod);
                 result=this.allProduitInfo.executeQuery();
                 while(result.next()){
listOfProduit.add((new Produit(result.getInt("Id"),result.getString("Nom"),result.getString("Description"),result.getString("Image"),result.getInt("prix"))));
                 }
             }
             
       catch (SQLException ex)
        {
            Logger.getLogger(GetWayCategorie.class.getName()).log(Level.SEVERE, null, ex);
        }
               
    return listOfProduit ;
}
    
     public Produit ListProduit(String nomprod) {
             Produit prod =new Produit();
             try{
                 this.allProduitInfo.setString(1,nomprod);
                 result=this.allProduitInfo.executeQuery();
                 
                 while(result.next()){
                   prod.setNom(result.getString("Nom"));
                prod.setId(result.getInt("Id"));
                prod.setPrix(result.getInt("prix"));
                prod.setDescription(result.getString("Description"));
                prod.setImage(result.getString("Image"));
                 }
               
                
             }
             
       catch (SQLException ex)
        {
            Logger.getLogger(GetWayCategorie.class.getName()).log(Level.SEVERE, null, ex);
        }
               
    return prod ;
}
    
}
