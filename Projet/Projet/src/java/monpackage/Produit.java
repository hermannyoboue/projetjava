package monpackage;



/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Anne
 */
public class Produit {
    private int Id;
    private String Nom;
    private String Description;
    private String Image;
    private int prix;

    
    
    public void setId(int id){
        this.Id=id;
    }
    public int getId(){
        return this.Id;
    }
    public void setNom(String nom){
        this.Nom=nom;
    }
    public String getNom(){
        return this.Nom;
    }
    public void setDescription(String desc){
        this.Description=desc;
    }
    public String getDescription(){
        return this.Description;
    }
    public void setImage(String img){
        this.Image=img;
    }
    public String getImage(){
        return this.Image;
    }
    public void setPrix(int prix){
        this.prix=prix;
    }
    public int getPrix(){
        return this.prix;
    }
    
    public Produit(){
        this.Id=0;
        this.Nom="";
        this.Description="";
        this.Image="";
        
    }
    
     public Produit(int id, String nom,String desc,String img,int prix){
        this.Id=id;
        this.Nom=nom;
        this.Description=desc;
        this.Image=img;   
        this.prix=prix;
    }
    
    
}
